<?php
class Question {
    public $number;
    public $question;
    public $answers;
    private $correct_answer;

    public function __construct($number, $question, $answers, $correct_answer){
        $this->number = $number;
        $this->question = $question;
        $this->answers = $answers;
        $this->correct_answer = $correct_answer;
    }

    public function get_correct(){
        return $this->correct_answer;
    }
}

$questions_page_1 = array(
    new Question(1, "What is the capital of France?", array("Paris", "London", "Berlin", "Madrid"), 1),
    new Question(2, "What is the capital of Germany?", array("Paris", "London", "Berlin", "Madrid"), 3),
    new Question(3, "What is the capital of Spain?", array("Paris", "London", "Berlin", "Madrid"), 4),
    new Question(4, "What is the capital of England?", array("Paris", "London", "Berlin", "Madrid"), 2),
    new Question(5, "What is the capital of Italy?", array("Paris", "London", "Berlin", "Madrid"), 1),
);


$questions_page_2 = array(
    new Question(6, "What is the capital of Portugal?", array("Paris", "London", "Berlin", "Madrid"), 3),
    new Question(7, "What is the capital of Poland?", array("Paris", "London", "Berlin", "Madrid"), 4),
    new Question(8, "What is the capital of Greece?", array("Paris", "London", "Berlin", "Madrid"), 2),
    new Question(9, "What is the capital of Sweden?", array("Paris", "London", "Berlin", "Madrid"), 1),
    new Question(10, "What is the capital of Norway?", array("Paris", "London", "Berlin", "Madrid"), 3),
);

$all_correct_answers = array();
foreach(array_merge($questions_page_1, $questions_page_2) as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}
?>
